@ECHO OFF
setlocal enabledelayedexpansion

pushd %~dp0

REM Command file for {{ cookiecutter.name }}

IF EXIST build/ rm -rf build/
rm -f *.spec

IF "%1" == "" GOTO :help
IF "%1" == "exe" GOTO :warning
IF "%1" == "cli" GOTO :warning
IF "%1" == "gui" GOTO :warning
IF "%1" == "wheel" GOTO :wheel
IF "%1" == "l10n" GOTO :l10n


:help
ECHO.[96mUsage[0m: [1mmake [COMMAND] [OPTION][0m
ECHO.
ECHO.  [93m{{ cookiecutter.name }}[0m
ECHO.  [94mVersion {{ cookiecutter.version }}[0m
ECHO.
ECHO.[96mOptions[0m:
ECHO.  [93m--yes  [0mBypass confirmation.
ECHO.
ECHO.[96mCommands[0m:
ECHO.  [93mwheel  [0mBuild wheel distribution.
ECHO.  [93mcli    [0mBuild windows executable with console.
ECHO.  [93mgui    [0mBuild windows executable without console.
ECHO.  [93mexe    [0mBuild both windows executable.
ECHO.  [93ml10n   [0mBuild localization language.
GOTO :cleanup


:warning
IF NOT "%2" == "--yes" (
	ECHO.
	ECHO. [101;93m                                                                              [0m
	ECHO. [101;93m                         * * *   W A R N I N G   * * *                        [0m
	ECHO. [101;93m                                                                              [0m
	ECHO.
	ECHO.Due to limitation in bumpversion, you must change version in ".ffi"
	ECHO.file manually.
	ECHO.
	SET /p continue="[93mDo you want to continue?[0m [y/N]: "
	IF /i "!continue!" neq "Y" GOTO :end
)

CALL :l10n

IF "%1" == "exe" (
	CALL :compile console {{ cookiecutter.package.title() }}
	CALL :compile windowed {{ cookiecutter.package.title() }}-W
)
IF "%1" == "cli" CALL :compile console {{ cookiecutter.package.title() }}
IF "%1" == "gui" CALL :compile windowed {{ cookiecutter.package.title() }}-W
GOTO :cleanup


:compile
pyinstaller ^
	--onefile ^
	--%1 ^
	--name=%2 ^
	--icon=src/{{ cookiecutter.package }}/res/icons/{{ cookiecutter.package }}.ico ^
	--hidden-import=colorlog ^
	--hidden-import=pkg_resources.py2_warn ^
	--version-file=.ffi ^
	--add-data=src\{{ cookiecutter.package }}\res;.\{{ cookiecutter.package }}\res ^
	--add-data=src\{{ cookiecutter.package }}\locale\id\LC_MESSAGES\{{ cookiecutter.package }}.mo;.\locale\id\LC_MESSAGES\ ^
	src/{{ cookiecutter.package }}/main.py
GOTO :EOF

:wheel
python setup.py bdist_wheel
GOTO :cleanup


:l10n
IF NOT EXIST src\{{ cookiecutter.package }}\locale mkdir src\{{ cookiecutter.package }}\locale
pybabel ^
    extract ^
    --keywords=t ^
    --project="{{ cookiecutter.name }}" ^
    --version="{{ cookiecutter.version }}" ^
    --copyright-holder="{{ cookiecutter.author }}" ^
    --msgid-bugs-address="{{ cookiecutter.email }}" ^
    --output-file=src\{{ cookiecutter.package }}\locale\.pot ^
    src\{{ cookiecutter.package }}
IF EXIST src\{{ cookiecutter.package }}\locale\id\LC_MESSAGES\{{ cookiecutter.package }}.po (
    pybabel update -i src\{{ cookiecutter.package }}\locale\.pot -D {{ cookiecutter.package }} -d src\{{ cookiecutter.package }}\locale
) else (
    pybabel init -i src\{{ cookiecutter.package }}\locale\.pot -D {{ cookiecutter.package }} -d src\{{ cookiecutter.package }}\locale -l id
)
pybabel compile -D {{ cookiecutter.package }} -d src\{{ cookiecutter.package }}\locale
GOTO :cleanup


:cleanup
IF EXIST build/ rm -rf build/
rm -f *.spec
popd
