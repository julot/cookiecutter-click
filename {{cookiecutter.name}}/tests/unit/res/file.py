import {{ cookiecutter.package }}.res


def test():
    env = {{ cookiecutter.package }}.res.File('configs/.env')
    assert env.anchor == '{{ cookiecutter.package }}.res.configs'
    assert env.name == '.env'

    authors = {{ cookiecutter.package }}.res.File('authors.rst')
    assert authors.anchor == '{{ cookiecutter.package }}.res'
    assert authors.name == 'authors.rst'

    rst = {{ cookiecutter.package }}.res.File('docs/misc/test.rst')
    assert rst.anchor == '{{ cookiecutter.package }}.res.docs.misc'
    assert rst.name == 'test.rst'
