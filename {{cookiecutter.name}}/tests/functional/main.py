import click.testing

import {{ cookiecutter.package }}.commands


def test():
    runner = click.testing.CliRunner()
    help_result = runner.invoke({{ cookiecutter.package }}.commands.group, ['--help'])
    assert help_result.exit_code == 0
    assert 'Show this message and exit.' in help_result.output
