Welcome to {{ cookiecutter.name }}'s documentation!
============================{% for _ in cookiecutter.name %}={% endfor %}

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   miscellaneous
{%- if cookiecutter.open_source_license != 'Not open source' %}
   contributing
{%- endif %}
   authors
   history


Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
