{% set is_open_source = cookiecutter.open_source_license != 'Not open source' -%}
{% for _ in cookiecutter.name %}={% endfor %}
{{ cookiecutter.name }}
{% for _ in cookiecutter.name %}={%- endfor %}

{% if is_open_source -%}
|PyPI version| |PyPI pyversions| |PyPI license| |PyPI status| |PyPI format|
|Travis| |Read The Docs|

.. |PyPI version| image:: https://img.shields.io/pypi/v/{{ cookiecutter.package }}.svg
   :target: https://pypi.org/project/{{ cookiecutter.package }}

.. |PyPI pyversions| image:: https://img.shields.io/pypi/pyversions/{{ cookiecutter.package }}.svg
   :target: https://pypi.org/project/{{ cookiecutter.package }}

.. |PyPI license| image:: https://img.shields.io/pypi/l/{{ cookiecutter.package }}.svg
   :target: https://pypi.org/project/{{ cookiecutter.package }}

.. |PyPI status| image:: https://img.shields.io/pypi/status/{{ cookiecutter.package }}.svg
   :target: https://pypi.org/project/{{ cookiecutter.package }}

.. |PyPI format| image:: https://img.shields.io/pypi/format/{{ cookiecutter.package }}.svg
   :target: https://pypi.org/project/{{ cookiecutter.package }}

.. |Travis| image:: https://img.shields.io/travis/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.package }}.svg
        :target: https://travis-ci.org/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.package }}

.. |Read The Docs| image:: https://readthedocs.org/projects/{{ cookiecutter.package | replace("_", "-") }}/badge/?version=latest
        :target: https://{{ cookiecutter.package | replace("_", "-") }}.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


{% endif -%}


{{ cookiecutter.description }}

{% if is_open_source %}
* Free software: {{ cookiecutter.open_source_license }}
{% else %}
{{ cookiecutter.copyright.replace('(c)', '©').replace('(C)', '©') }}
{%- endif %}


Installation
============

To install, run this command in terminal:

.. code-block::

   {%- if is_open_source %}

   > python -m pip install {{ cookiecutter.package }}
   {%- else %}

   > python -m pip install https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.package }}/-/archive/master/{{ cookiecutter.package }}-master.tar.gz
   {%- endif %}


This is the preferred method to install {{ cookiecutter.name }},
as it will always install the most recent stable release.

If you don't have `pip`_ installed,
this `Python installation guide`_ can guide you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


Usage
=====

.. code-block:: bat

   > {{ cookiecutter.package }} --help

Full documentation is `here`_.

.. _here: https://{{ cookiecutter.package | replace("_", "-") }}.readthedocs.io
