import logging

import click
import click_rich_help
import rich_rst

import {{ cookiecutter.package }}
import {{ cookiecutter.package }}.res
import {{ cookiecutter.package }}.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme={{ cookiecutter.package }}.rich.theme,
    short_help='See authors.',
)
def command() -> None:
    """
    See authors.

    """
    log = logging.getLogger(__name__)
    log.info('{{ cookiecutter.package.title() }}->Authors command started.')
    try:
        run()
    except click.Abort:
        click.secho('\nAborted!', fg='red')
    except Exception as exc:
        {{ cookiecutter.package }}.rich.console.print(exc)
        log.error(exc, exc_info=exc, stack_info=True)
    log.info('{{ cookiecutter.package.title() }}->Authors command finished.')


def run() -> None:
    {{ cookiecutter.package }}.rich.console.print(rich_rst.RestructuredText(
        {{ cookiecutter.package }}.res.File('authors.rst').path.read_text()
    ))
