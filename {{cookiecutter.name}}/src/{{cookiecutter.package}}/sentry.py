import logging

import click
import click_rich_help
import ruamel.yaml

import {{ cookiecutter.package }}.config
import {{ cookiecutter.package }}.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme={{ cookiecutter.package }}.rich.theme,
    short_help='Send error log to [primary]sentry.io[/] for testing purpose.',
)
def command() -> None:
    """
    Send error log to [primary]sentry.io[/] to test whether sentry is working
    or not.

    Please make sure that sentry is [success]enabled[/] in config.

    """
    log = logging.getLogger(__name__)
    log.info('{{ cookiecutter.package.title() }}->Sentry command started.')
    try:
        run()
    except click.Abort:
        click.secho('\nAborted!', fg='red')
    except Exception as exc:
        {{ cookiecutter.package }}.rich.console.print(exc)
        log.error(exc, exc_info=exc, stack_info=True)
    log.info('{{ cookiecutter.package.title() }}->Sentry command finished.')


def run() -> None:
    yaml = {{ cookiecutter.package }}.config.Path('.yml')

    if not yaml.instance.exists():
        {{ cookiecutter.package }}.rich.console.print(
            'Configuration file is missing. '
            'Please create one using [info]config[/] command.'
        )
        return

    config = ruamel.yaml.YAML(typ='safe').load(yaml.instance)

    if not config['sentry']['enabled']:
        {{ cookiecutter.package }}.rich.console.print(
            'Sentry is [danger]disabled[/] in configuration file.'
        )
        {{ cookiecutter.package }}.rich.console.print(
            f'Please [success]enable[/] sentry in [info]{yaml.instance}[/] '
            'configuration file.'
        )
        return

    logging.getLogger(__name__).error(
        'Error log example send from '
        f'{% raw %}{{% endraw %}{{ cookiecutter.package }}.__name__{% raw %}}{% endraw %}  Version {% raw %}{{% endraw %}{{ cookiecutter.package }}.__version__{% raw %}}{% endraw %}'
    )
