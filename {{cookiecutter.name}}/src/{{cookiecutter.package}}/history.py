import logging

import click
import click_rich_help
import rich_rst

import {{ cookiecutter.package }}
import {{ cookiecutter.package }}.res
import {{ cookiecutter.package }}.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme={{ cookiecutter.package }}.rich.theme,
    short_help='See version history.',
)
def command() -> None:
    """
    See version history.

    """
    log = logging.getLogger(__name__)
    log.info('{{ cookiecutter.package.title() }}->History command started.')
    try:
        run()
    except click.Abort:
        click.secho('\nAborted!', fg='red')
    except Exception as exc:
        {{ cookiecutter.package }}.rich.console.print(exc)
        log.error(exc, exc_info=exc, stack_info=True)
    log.info('{{ cookiecutter.package.title() }}->History command finished.')


def run() -> None:
    {{ cookiecutter.package }}.rich.console.print(rich_rst.RestructuredText(
        {{ cookiecutter.package }}.res.File('history.rst').path.read_text()
    ))
