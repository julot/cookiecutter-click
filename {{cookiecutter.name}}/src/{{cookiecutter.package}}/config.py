import functools
import logging
import pathlib
import shutil

import click
import click_rich_help
import platformdirs

import {{ cookiecutter.package }}
import {{ cookiecutter.package }}.res
import {{ cookiecutter.package }}.rich


class Path(object):

    def __init__(self, name: str) -> None:
        """
        Convenience object to resource file in {{ cookiecutter.package }}.res.configs.

        :param name: The config file name.
        :type name: str

        """
        self.name = name

    @functools.cached_property
    def default(self) -> pathlib.Path:
        """
        Path to default config file.

        """
        return {{ cookiecutter.package }}.res.File(f'configs/{self.name}').path

    @functools.cached_property
    def instance(self) -> pathlib.Path:
        """
        Path to config file in pyInstaller executable if the application is
        frozen or user config directory in any other case.

        """
        return (
            {{ cookiecutter.package }}.PATH
            if {{ cookiecutter.package }}.FROZEN
            else platformdirs.user_config_path(
                {{ cookiecutter.package }}.__title__,
                {{ cookiecutter.package }}.__author__,
            )
        ) / self.name


class StyledCommand(click_rich_help.StyledCommand):

    def format_help_text(
        self,
        ctx: click.Context,
        formatter: click.HelpFormatter,
    ) -> None:
        super().format_help_text(ctx, formatter)
        formatter.write_paragraph()
        with formatter.indentation():
            config = Path('.yml')
            formatter.write_text(
                'The configuration file will be saved to '
                f'[primary]{config.instance.parent}[/]'
            )


@click.command(
    cls=StyledCommand,
    theme={{ cookiecutter.package }}.rich.theme,
    short_help='Configure {{ cookiecutter.package.title() }}.',
)
@click.confirmation_option(
    prompt='Overwrite existing configuration file if exists?',
)
def command() -> None:
    """
    Create configuration file .yml and .env with default value.

    """
    log = logging.getLogger(__name__)
    log.info('{{ cookiecutter.package.title() }}->Config command started.')
    try:
        run()
    except click.Abort:
        click.secho('\nAborted!', fg='red')
    except Exception as exc:
        {{ cookiecutter.package }}.rich.console.print(exc)
        log.error(exc)
    log.info('{{ cookiecutter.package.title() }}->Config command finished.')


def run() -> None:
    yml = Path('.yml')
    shutil.copy(yml.default, yml.instance)
    {{ cookiecutter.package }}.rich.console.print(
        f'[info].yml[/] file created at [info]{yml.instance}[/]'
    )

    env = Path('.env')
    shutil.copy(env.default, env.instance)
    {{ cookiecutter.package }}.rich.console.print(
        f'[info].env[/] file created at [info]{env.instance}[/]'
    )
