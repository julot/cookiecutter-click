import builtins
import contextlib
import logging
import logging.config
import pathlib
import sys

import click
import click_rich_help
import platformdirs
import ruamel.yaml
import toga
import toga.style
import travertino.constants

import {{ cookiecutter.package }}
import {{ cookiecutter.package }}.author
import {{ cookiecutter.package }}.config
import {{ cookiecutter.package }}.history
import {{ cookiecutter.package }}.res
import {{ cookiecutter.package }}.rich
import {{ cookiecutter.package }}.sentry


class Toga(toga.App):

    def startup(self) -> None:
        self.main_window = toga.MainWindow()
        self.main_window.content = toga.Box(
            children=[
                toga.Box(
                    style=toga.style.Pack(
                        direction=travertino.constants.COLUMN,
                        flex=1,
                    ),
                    children=[
                        toga.Label(
                            (
                                '{{ cookiecutter.package.title() }} invoked without sub command.\n'
                                'Please run the CLI version for more '
                                'information'
                            ),
                            style=toga.style.Pack(
                                padding=20,
                                text_align=travertino.constants.CENTER,
                            ),
                        ),
                    ],
                ),
            ],
            style=toga.style.Pack(
                alignment=travertino.constants.CENTER,
                direction=travertino.constants.ROW,
            ),
        )
        self.main_window.show()


@click.group(
    cls=click_rich_help.StyledGroup,
    theme={{ cookiecutter.package }}.rich.theme,
    invoke_without_command=True,
)
@click.version_option(version={{ cookiecutter.package }}.__version__, message='%(version)s')
@click.pass_context
def group(ctx) -> None:
    """
    \b
    [option]{{ cookiecutter.name }}[/]
    [primary]Version {{ cookiecutter.version }}[/]

    {{ cookiecutter.description }}

    [success]{{ cookiecutter.copyright.replace('(c)', '©').replace('(C)', '©') }}[/]

    """
    if ctx.invoked_subcommand is None:
        log = logging.getLogger(__name__)
        log.info('{{ cookiecutter.package.title() }} started.')

        msg = '{{ cookiecutter.package.title() }} invoked without sub command.\n'
        if sys.stdout and sys.stdout.isatty():
            # Entry point is console_scripts
            {{ cookiecutter.package }}.rich.console.print(msg, style='info')
            {{ cookiecutter.package }}.rich.console.print(
                'Type [warning]{{ cookiecutter.package }} --help[/] for more information.'
            )
        else:
            # Entry point is gui_scripts, headless python (without console)
            app = Toga(
                {{ cookiecutter.package }}.__title__,
                'test.{{ cookiecutter.author.lower().replace(' ', '-') }}.{{ cookiecutter.package }}',
                icon={{ cookiecutter.package }}.res.File('icons/{{ cookiecutter.package }}.ico').path,
            )
            app.main_loop()

        log.info('{{ cookiecutter.package.title() }} stopped.')


group.add_command({{ cookiecutter.package }}.author.command, name='author')
group.add_command({{ cookiecutter.package }}.config.command, name='config')
group.add_command({{ cookiecutter.package }}.history.command, name='history')
group.add_command({{ cookiecutter.package }}.sentry.command, name='sentry')


def run():
    yaml = {{ cookiecutter.package }}.config.Path('.yml')
    config = ruamel.yaml.YAML(typ='safe').load(
        yaml.instance if yaml.instance.exists() else yaml.default
    )

    with contextlib.suppress(KeyError):
        original_log_path = pathlib.Path(
            config['logging']['handlers']['file']['filename']
        )
        if not original_log_path.is_absolute():
            log_path = (
                {{ cookiecutter.package }}.PATH
                if {{ cookiecutter.package }}.FROZEN
                else platformdirs.user_log_path(
                    {{ cookiecutter.package }}.__title__,
                    {{ cookiecutter.package }}.__author__,
                )
            ) / original_log_path.name
            config['logging']['handlers']['file']['filename'] = str(log_path)
            log_path.parent.mkdir(parents=True, exist_ok=True)

    logging.config.dictConfig(config['logging'])

    log = logging.getLogger(__name__)

    if not config['sentry']['enabled']:
        log.warning('Sentry is disabled.')
    else:
        log.info('Sentry is enabled.')

        import sentry_sdk
        import sentry_sdk.integrations.logging

        sentry_logging = sentry_sdk.integrations.logging.LoggingIntegration(
            level=logging.ERROR,  # Capture info and above as breadcrumbs
            event_level=logging.ERROR,  # Send errors as events
        )
        sentry_sdk.init(
            dsn=config['sentry']['dsn'],
            integrations=[
                sentry_logging,
            ],
            traces_sample_rate=config['sentry']['traces_sample_rate'],
        )

    try:
        import icecream

        icecream.install()
        log.info(
            'IceCream installed to built-ins successfully. '
            'ic command is now available globally.'
        )

    except ImportError:  # Graceful fallback if IceCream isn't installed.
        builtins.ic = (
            lambda *a: None
            if not a
            else (a[0] if len(a) == 1 else a)
        )
        log.info(
            'IceCream module is not exists. '
            'ic command will fallback to produce nothing.'
        )

    del config

    group()


if __name__ == "__main__":
    run()
