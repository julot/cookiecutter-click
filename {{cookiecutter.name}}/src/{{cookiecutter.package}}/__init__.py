# Because I like to extend 3rd party library by creating module/package here,
# all imported 3rd party library here should be prefixed with underscore.

import gettext as _gettext
import os as _os
import pathlib as _pathlib
import sys as _sys

import dotenv as _dotenv
import platformdirs as _platformdirs

from . import res as _res


__title__ = '{{ cookiecutter.name }}'
__version__ = '{{ cookiecutter.version }}'
__description__ = '{{ cookiecutter.description }}'
__author__ = '{{ cookiecutter.author }}'
__email__ = '{{ cookiecutter.email }}'
__copyright__ = '{{ cookiecutter.copyright.replace('(c)', '©').replace('(C)', '©') }}'

FROZEN: bool = getattr(_sys, 'frozen', False)
"""Whether the app is pyInstaller executable format or not."""

PATH: _pathlib.Path = (
    _pathlib.Path(_sys.executable).parent if FROZEN
    else _pathlib.Path(_os.getcwd())
)
"""
Path to pyInstaller executable file directory or current working directory.

"""

_dotenv_path = (
    PATH
    if FROZEN
    else _platformdirs.user_config_path(__title__, __author__)
) / '.env'
if _dotenv_path.exists():
    _dotenv.load_dotenv(_dotenv_path)
else:
    _dotenv.load_dotenv(_res.File('configs/.env').path)

# https://docs.python.org/3/library/gettext.html#class-based-api
try:
    # * Support for executable produced by PyInstaller
    _module_dir = getattr(_sys, '_MEIPASS')
except AttributeError:
    _module_dir = _os.path.dirname(__file__)
translation = _gettext.translation(
    '{{ cookiecutter.package }}',
    localedir=_pathlib.Path(_module_dir) / 'locale',
    fallback=True,
    languages=[_os.environ.get('LANGUAGE', 'en')],
)
"""
Translation object.

Usage: translation.gettext('Hello World!')
"""
